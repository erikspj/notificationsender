﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NotificationForm.aspx.cs" Inherits="NotificationSender.NotificationForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #txtShortMessage {
            width: 927px;
            height: 17px;
        }
        .auto-style1 {
            width: 682px;
        }
        .auto-style2 {
            width: 486px;
        }
    </style>
</head>
<body>
    <form id="frmNotification" runat="server">
        <div>
            <table style="width: 50%;">
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lblReceiver" runat="server" Text="Receiver"></asp:Label>
                    </td>
                    <td class="auto-style1">

                        <asp:DropDownList ID="ddlReceiver" runat="server" Height="25px" Width="124px">
                        </asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style2"><asp:Label ID="lblShortMessage" runat="server" Text="Short Message" Visible="False"></asp:Label></td>
                    <td class="auto-style1">
                        <input id="txtShortMessage" runat="server" type="text" visible="True" /></td>
                </tr>
                
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style1">
                        <asp:Button ID="btnSendAlerts" runat="server" Text="Send Alerts" OnClick="btnSendAlerts_Click" />
                        <asp:Button ID="btnSendNotifications" runat="server" OnClick="btnSendNotifications_Click" Text="Send Periodic Notification" />
                        <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" />
                    </td>
                </tr>
                
            </table>
        </div>
    </form>
</body>
</html>
