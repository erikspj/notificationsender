﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;

namespace NotificationSender
{
    public class DiggerInNotification
    {
        public int emailId { get; private set; }
        public string type { get; private set; }
        public int uMrId { get; private set; }
        public string subject { get; private set; }
        public string body { get; private set; }
        public long createdDate { get; private set; }
        public bool isRead { get; private set; }
        public string vName { get; private set; }
        public int algId { get; private set; }

        public DiggerInNotification()
        { }

        public DiggerInNotification(DataRow row)
        {
            emailId = int.Parse(row["ID"].ToString());
            type = row["eType"].ToString();
            uMrId = int.Parse(row["U_MR_ID"].ToString());
            subject = row["eSubject"].ToString();
            body = doubleQuotesToSingleQuotes(row["eBody"].ToString());
            createdDate = (long) (DateTime.Parse(row["eCreatedDate"].ToString(), new CultureInfo("no")) - UNIX_EPOCH).TotalMilliseconds;
            isRead = (bool) row["eISRead"];
            vName = row["vName"].ToString();
            algId = int.Parse(row["algID"].ToString());
        }

        private string hexEncode(string text)
        {
            //For example, '#', '%', '\', '?' should be replaced by %23, %25, %27, %3f respectively. 

            return text.Replace("#", "%23").Replace("%", "%25").Replace("?", "%3f").Replace("&", "%26");
        }

        private string doubleQuotesToSingleQuotes(string doubleQuotes)
        {
            return doubleQuotes.Replace("\"", "'")
                .Replace("'//", "'http://") // because of a bug in the RCChart field: 'http:' is sometimes missing
                .Replace("href='salesdata", "href='http://salesdata")
                .Replace("href='diggerintest", "href='http://diggerintest");
        }

        /// <summary>
        /// Constant to convert from C# Epoch (0001-01-01) to Java Epoch (1970-01-01)
        /// </summary>
        private readonly DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0);
    }
}