﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NotificationSender
{
    public partial class NotificationForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            fillReceiver();
        }

        private void fillReceiver()
        {
            DataTable dtReceivers = getReceivers();

            foreach (DataRow drReceiver in dtReceivers.Rows)
            {
                if (ddlReceiver.Items.FindByText(drReceiver["UserName"].ToString()) == null)
                    ddlReceiver.Items.Add(drReceiver["UserName"].ToString());
            }

            // mockup
            ddlReceiver.SelectedIndex = ddlReceiver.Items.IndexOf(ddlReceiver.Items.FindByText("Erik"));
        }

        private DataTable getReceivers()
        {
            DAL objDal = new DAL();
            DataTable dtReceivers = objDal.GetDataQry("SELECT UserName FROM Devices D LEFT JOIN Users U ON D.UserId=U.UserId",
                ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString).Tables[0];

            return dtReceivers;
        }

        private string getUserId(string userName)
        {
            DAL objDal = new DAL();
            DataTable dtUsers = objDal.GetDataQry("SELECT UserId FROM Users WHERE UserName = '" + userName + "'",
                ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString).Tables[0];

            return dtUsers.Rows[0]["UserId"].ToString();
        }

        protected void btnSendAlerts_Click(object sender, EventArgs e)
        {
            txtShortMessage.Value = "";
            sendNotifications("Alert");
        }

        private void sendNotifications(string type)
        {
            string apiKey = ConfigurationManager.AppSettings["apiKey"];
            string userId = getUserId(ddlReceiver.SelectedValue);
            DiggerInNotification[] notifications = GetnUserMReMails(userId, type);
            int successCounter = 0;
            int failureCounter = 0;

            StringCollection devices = getDevices(userId);
            StringBuilder sbDevices = new StringBuilder("\"");
            for (int i = 0; i < devices.Count; i++)
                sbDevices.Append((i > 0 ? "\",\"" : "") + devices[i]);

            sbDevices.Append("\"");

            foreach (DiggerInNotification din in notifications)
            {
                string postData =
                    "{ \"registration_ids\": [ " + sbDevices + " ]" +
                    ",\"data\": {\"emailId\": \"" + din.emailId + "\"" +
                    ",\"eType\": \"" + din.type + "\"" +
                    ",\"uMRId\": \"" + din.uMrId + "\"" +
                    ",\"eSubject\": \"" + din.subject + "\"" +
                    ",\"eBody\": \"" + din.body + "\"" +
                    ",\"eIsRead\": \"" + din.isRead + "\"" +
                    ",\"date\": \"" + din.createdDate + "\"" +
                    ",\"vName\": \"" + din.vName + "\"" +
                    ",\"algId\": \"" + din.algId + "\"" +
                    "}}";

                incrementCounters(SendGCMNotification(apiKey, postData), ref successCounter, ref failureCounter);
            }

            txtShortMessage.Value = String.Format("Success: {0}, Failure: {1}", successCounter, failureCounter);
        }

        private void incrementCounters(string result, ref int i, ref int j)
        {
            i += getValue(result, "success");
            j += getValue(result, "failure");
        }

        private int getValue(string result, string key)
        {
            int start = result.IndexOf(key) + key.Length + 2;
            int end = result.IndexOf(',', start) - start;

            return (int.Parse(result.Substring(start, end)));
        }

        private DiggerInNotification[] GetnUserMReMails(string userId, string type)
        {
            DAL objDal = new DAL();
            string query =
@"SELECT [ID]
      ,[eType]
	  ,[U_MR_ID]
      ,[eSubject]
      ,[eBody]
      ,[eCreatedDate]
      ,[eISRead]
      ,[vName]
      ,[algID]
  FROM [dbo].[nUser_MR_eMails]
  WHERE [eType]='" + type +
                   "' AND [UserID]='" + userId +
                   "' AND [eISRead] = 0"; // unread
            DataSet dsEmails =
                objDal.GetDataQry(query, ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString);

            if (dsEmails != null && dsEmails.Tables.Count > 0)
            {
                List<DiggerInNotification> notifications = new List<DiggerInNotification>();

                foreach (DataRow row in dsEmails.Tables[0].Rows)
                {
                    notifications.Add(new DiggerInNotification(row));
                }

                return notifications.ToArray();
            }

            return null;
        }


        private string SendGCMNotification(string apiKey, string postData, string postDataContentType = "application/json")
        {
            ServicePointManager.ServerCertificateValidationCallback +=
                new RemoteCertificateValidationCallback(ValidateServerCertificate);

            //  MESSAGE CONTENT
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            //  CREATE REQUEST
            HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://android.googleapis.com/gcm/send");

            Request.Method = "POST";
            Request.KeepAlive = false;
            Request.ContentType = postDataContentType;
            Request.Headers.Add(string.Format("Authorization: key={0}", apiKey));
            Request.ContentLength = byteArray.Length;

            Stream dataStream = Request.GetRequestStream();

            dataStream.Write(byteArray, 0, byteArray.Length);

            dataStream.Close();

            //  SEND MESSAGE
            try
            {
                WebResponse Response = Request.GetResponse();
                HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;

                if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
                {
                    txtShortMessage.Value = "Unauthorized - need new token";
                }
                else if (!ResponseCode.Equals(HttpStatusCode.OK))
                {
                    txtShortMessage.Value = "Response from web service isn't OK";
                }

                StreamReader Reader = new StreamReader(Response.GetResponseStream());
                string responseLine = Reader.ReadToEnd();

                Reader.Close();

                return responseLine;
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        public static bool ValidateServerCertificate(object sender,
            X509Certificate certificate,
            X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private StringCollection getDevices(string userId)
        {
            DAL objDal = new DAL();
            string sql = "SELECT * FROM Devices WHERE UserId = '" + userId + "'";
            DataTable dt = objDal.GetDataQry(sql,
                System.Configuration.ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString).Tables[0];
            StringCollection devices = new StringCollection();

            foreach (DataRow drDevices in dt.Rows)
            {
                devices.Add(drDevices["DeviceId"].ToString());
            }

            return devices;
        }

        protected void btnSendNotifications_Click(object sender, EventArgs e)
        {
            txtShortMessage.Value = "";
            sendNotifications("Periodic Notification");
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtShortMessage.Value = "";
        }
    }
}