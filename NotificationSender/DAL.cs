﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace NotificationSender
{
    public class DAL
    {

        public DataSet GetDataQry(string SQLQuery, string ConnectionString)
        {

            string queryString = SQLQuery;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryString;
                command.CommandType = CommandType.Text;

                try
                {
                    connection.Open();

                    SqlDataAdapter sqlAdpt = new SqlDataAdapter(command);
                    DataSet ds = new DataSet();
                    sqlAdpt.Fill(ds);

                    return ds;
                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
            return null;
        }

        public DataSet GetDataProc(string ProcName, string ConnectionString)
        {

            string queryString = ProcName;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryString;
                command.CommandType = CommandType.StoredProcedure;

                try
                {
                    connection.Open();

                    SqlDataAdapter sqlAdpt = new SqlDataAdapter(command);
                    DataSet ds = new DataSet();
                    sqlAdpt.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }


            }
            return null;
        }

        public DataSet GetDataProc(string ProcName, string ConnectionString, string[] sqlPrmColl)
        {

            string queryString = ProcName;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryString;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 0;
                for (int i = 0; i <= sqlPrmColl.Length - 1; i = i + 4)
                {

                    SqlParameter sqlParam = new SqlParameter();
                    sqlParam.ParameterName = sqlPrmColl[i + 0].ToString();

                    if (sqlPrmColl[i + 1] == "DbType.Int32")
                    {
                        sqlParam.DbType = DbType.Int32;
                        sqlParam.Value = Convert.ToInt32(sqlPrmColl[i + 3]);

                    }
                    else if (sqlPrmColl[i + 1] == "DbType.String")
                    {
                        sqlParam.DbType = DbType.String;
                        sqlParam.Value = sqlPrmColl[i + 3];

                    }
                    else if (sqlPrmColl[i + 1] == "DbType.DateTime")
                    {
                        sqlParam.DbType = DbType.DateTime;
                        sqlParam.Value = sqlPrmColl[i + 3];

                    }
                    sqlParam.Size = Convert.ToInt32(sqlPrmColl[i + 2]);


                    command.Parameters.Add(sqlParam);
                }

               // try
               // {
                    connection.Open();

                    SqlDataAdapter sqlAdpt = new SqlDataAdapter(command);
                    DataSet ds = new DataSet();
                    sqlAdpt.Fill(ds);
                    return ds;
              //  }
               // catch (Exception ex)
              //  {
                 //   throw (ex);
              //  }
            }
            return null;
        }


        static private string GetConnectionString()
        {
            // To avoid storing the connection string in your code, 
            // you can retrieve it from a configuration file.
            return ConfigurationManager.ConnectionStrings[0].ToString();
        }

        public string[] SaveDataProc(string ProcName, string ConnectionString, string[] sqlPrmColl)
        {

            string queryString = ProcName;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = ProcName;
                command.CommandType = CommandType.StoredProcedure;

                for (int i = 0; i <= sqlPrmColl.Length - 1; i = i + 4)
                {

                    SqlParameter sqlParam = new SqlParameter();
                    sqlParam.ParameterName = sqlPrmColl[i + 0].ToString();

                    if (sqlParam.ParameterName == "@ErrorCode")
                    {
                        sqlParam.Direction = ParameterDirection.Output;
                    }

                    if (sqlPrmColl[i + 1] == "DbType.Int32")
                    {
                        sqlParam.DbType = DbType.Int32;
                        sqlParam.Value = Convert.ToInt32(sqlPrmColl[i + 3]);

                    }
                    else if (sqlPrmColl[i + 1] == "DbType.String")
                    {
                        sqlParam.DbType = DbType.String;
                        sqlParam.Value = Convert.ToString(sqlPrmColl[i + 3]);

                    }
                    else if (sqlPrmColl[i + 1] == "DbType.DateTime")
                    {
                        sqlParam.DbType = DbType.DateTime;

                        if (sqlPrmColl[i + 3] != null)
                        {
                            sqlParam.Value = Convert.ToDateTime(sqlPrmColl[i + 3]);
                        }
                        else
                        {
                            sqlParam.Value = "1/1/1900";

                        }

                    }
                    sqlParam.Size = Convert.ToInt32(sqlPrmColl[i + 2]);


                    command.Parameters.Add(sqlParam);
                }

                string[] returnString = new string[2];

                try
                {
                    connection.Open();


                    returnString[0] = Convert.ToString(command.ExecuteScalar());

                  //  returnString[1] = Convert.ToString(command.Parameters["@ErrorCode"].Value);



                }
                catch (Exception ex)
                {
                   throw ex;

                }


                return returnString;
            }


        }

        public int SaveDataQry(string SqlQuery, string ConnectionString)
        {
            string queryString = SqlQuery;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryString;
                command.CommandType = CommandType.Text;


                // command.Parameters.Add(sqlprm);


                try
                {
                    connection.Open();

                    int i = Convert.ToInt32(command.ExecuteScalar());
                    return i;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }


            }
            return 0;
        }
    }
}